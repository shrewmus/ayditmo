-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 08, 2015 at 05:03 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ayditmo`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `user_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `type` int(11) NOT NULL,
  `description` text CHARACTER SET latin1,
  `rule_name` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `data` text CHARACTER SET latin1,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `idx_auth_item` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) CHARACTER SET latin1 NOT NULL,
  `child` varchar(64) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `data` text CHARACTER SET latin1,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) CHARACTER SET latin1 NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `municipality`
--

CREATE TABLE IF NOT EXISTS `municipality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_name` varchar(255) NOT NULL COMMENT 'название',
  `municipality_emblem` varchar(255) DEFAULT NULL COMMENT 'файл герба или null',
  `percent` varchar(255) NOT NULL DEFAULT '0' COMMENT 'процент',
  `last_edit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scope_prepdoc` text COMMENT 'Подготовка документов к обследованию',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Муниципалитеты' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `municipality_common`
--

CREATE TABLE IF NOT EXISTS `municipality_common` (
  `mcommon_id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) NOT NULL,
  `customer` mediumtext NOT NULL COMMENT 'Заказчик',
  `executor` mediumtext COMMENT 'Исполнитель',
  `work_description` text COMMENT 'Полное наименование работ',
  `goals` text COMMENT 'Цели',
  `tasks` text COMMENT 'Задачи',
  `work_list` text COMMENT 'Краткий состав работ',
  `survey_methods` text COMMENT 'Методика обследования',
  `survey_borders` text COMMENT 'Границы обследования',
  `abbreviations` text COMMENT 'Сокращения',
  PRIMARY KEY (`mcommon_id`),
  KEY `idx_municipality_common` (`municipality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ОБЩИЕ СВЕДЕНИЯ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `municipality_scope`
--

CREATE TABLE IF NOT EXISTS `municipality_scope` (
  `scope_id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) NOT NULL,
  `scope_name` varchar(255) NOT NULL COMMENT 'Название сферы деятельности',
  `curator` varchar(255) DEFAULT NULL COMMENT 'Куратор',
  `development_authority` text COMMENT 'Полномочия по развитию',
  `regular_count` int(11) DEFAULT NULL COMMENT 'Штатная численность',
  `org_structure` text COMMENT 'Организационная структура',
  `interview_info` text COMMENT 'Информация полученная в результате интерьвю',
  `basic_functions` text COMMENT 'Основные функции',
  `info_objects` text COMMENT 'Объекты учета, данные и информация, с которой работает подразделение',
  `scope_reports` text,
  `conclusion` text COMMENT 'Заключение',
  `scope_recomendations` text COMMENT 'Реккомендации',
  `scope_services` text COMMENT 'Предоставляемы муниципальные услуги',
  PRIMARY KEY (`scope_id`),
  KEY `idx_municipality_scope` (`municipality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `normative_legal_support`
--

CREATE TABLE IF NOT EXISTS `normative_legal_support` (
  `legal_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_result` text COMMENT 'Выявлено в ходе обследования',
  `legal_recommendations` text COMMENT 'Рекомендации',
  `municipality_id` int(11) NOT NULL,
  PRIMARY KEY (`legal_id`),
  KEY `idx_normative_legal_support` (`municipality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `org_structure`
--

CREATE TABLE IF NOT EXISTS `org_structure` (
  `orgstruct_id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_by` text COMMENT 'Кем принята',
  `committee_count` int(11) DEFAULT NULL COMMENT 'Комитеты: количество',
  `committee_regular` int(11) DEFAULT NULL COMMENT 'Коммитеты: Штатная численность',
  `teradmin_count` int(11) DEFAULT NULL COMMENT 'Территориальные управления: количество',
  `teradmin_regular` int(11) DEFAULT NULL COMMENT 'Территориальные управления: штатная численность',
  `admin_count` int(11) DEFAULT NULL COMMENT 'Управления: количество',
  `admin_regular` int(11) DEFAULT NULL COMMENT 'Управления: штатная численность',
  `department_count` int(11) DEFAULT NULL COMMENT 'Отделы: количество',
  `department_regular` int(11) DEFAULT NULL COMMENT 'Отделы: штатная численность',
  `all_regular_count` int(11) DEFAULT NULL COMMENT 'Общее штатное количество муниципальных служащих',
  `informations_authority` text COMMENT 'Полномочия и курирование ИКТ',
  `orgstruct_recommendations` text COMMENT 'Рекомендации',
  `municipality_id` int(11) NOT NULL,
  PRIMARY KEY (`orgstruct_id`),
  KEY `idx_org_structure` (`municipality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `fk_auth_item` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `municipality_common`
--
ALTER TABLE `municipality_common`
  ADD CONSTRAINT `fk_municipality_common` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `municipality_scope`
--
ALTER TABLE `municipality_scope`
  ADD CONSTRAINT `fk_municipality_scope` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `normative_legal_support`
--
ALTER TABLE `normative_legal_support`
  ADD CONSTRAINT `fk_normative_legal_support` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `org_structure`
--
ALTER TABLE `org_structure`
  ADD CONSTRAINT `fk_org_structure` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
