<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container site-login">
    <div class="row"><img src="img/u0_normal.png" />
    <h1>Исследования состояния информатизации<br/>
        Муниципальных образований<br/>
        Московской области
    </h1></div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' =>[
                    'template' => '{input}'
                ]
            ]); ?>
                <?= $form->field($model, 'username')->textInput(['placeholder'=>'Логин']) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Пароль']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>

</div>

