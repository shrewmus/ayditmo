<?php
/**
 * CompareAsset.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.01.15
 * Time: 17:01
 * Copyright 2015
 */

namespace backend\assets;


use yii\web\AssetBundle;

class CompareAsset extends AssetBundle{

    public $css = [
        'css/jquery.dataTables.css'
    ];

    public $js = [
//        'js/jquery.mousewheel.js',
        'js/jquery.dataTables.js',
        'js/dataTables.fixedColumns.js',
        'js/compareworks.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}