<?php
/**
 * MoformAsset.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/13/15
 * Time: 2:29 PM
 * Copyright 2015
 */

namespace backend\assets;


use yii\web\AssetBundle;

class MoformAsset extends AssetBundle {

    public $js = [
        'js/underscore-min.js',
        'js/formworks.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}