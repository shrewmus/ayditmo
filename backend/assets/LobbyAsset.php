<?php
/**
 * LobbyAsset.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.01.15
 * Time: 14:33
 * Copyright 2015
 */

namespace backend\assets;


use yii\web\AssetBundle;

class LobbyAsset extends AssetBundle{

    public $js = [
        'js/lobbyworks.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}