<?php
/**
 * _normativeLegal.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/7/15
 * Time: 1:43 AM
 * Copyright 2015
 */

use yii\helpers\Html;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>2. НОРМАТИВНО-ПРАВОВОЕ ОБЕСПЕЧЕНИЕ</h2>
    </div>
<?php

//модель \common\models\NormativeLegalSupport

?>
    <div class="panel-body">
        <?= $form->field($normativeLegal,'survey_result')->textarea(['rows'=>3]) ?>
        <div class="form-group field-normativelegalsupport-legal_recommendations">
            <?php
            echo Html::activeLabel($normativeLegal,'legal_recommendations');
            echo Html::activeTextarea($normativeLegal,'legal_recommendations',['class'=>'form-control','id'=>'legal_recommendations']);
            ?>
            <span class="btn btn-danger form-control remove-btn" id="remove_legal_recommendations">
                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
            </span>
        </div>
    </div>
</div>