<?php
/**
 * _orgStruct.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/7/15
 * Time: 1:49 AM
 * Copyright 2015
 */

use yii\helpers\Html;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>3. ОРГАНИЗАЦИОННО-ШТАТНАЯ СТРУКТУРА</h2>
    </div>
    <div class="panel-body">
        <?php

        //модель \common\models\OrgStructure

        ?>
        <?= $form->field($orgStructure,'approved_by')->textarea(['rows'=>2]) ?>
        <div style="position: relative; overflow: hidden">
            <?php
            $orgPartFields = ['committee' => 'Комитеты',
                              'teradmin'  => 'Терр..е управления',
                              'admin'=>'Управления',
                              'department'=>'Отделы'];
            foreach($orgPartFields as $key=>$strLabel){
                ?>
                <div class="form-group field-orgstructure_recommendations part-field" >
                    <?php
                    $count = $key . '_count';
                    $regular = $key . '_regular';
                    echo Html::label(
                        $strLabel, null, ['class' => 'part-field']
                    ); ?>
                    <span class="part-field" id="orgstruct_<?= $key ?>">
                        <?php
                        echo Html::activeInput(
                            'text', $orgStructure, $count,
                            ['class'       => 'part-field',
                             'placeholder' => $orgStructure->getAttributeLabel($count)]
                        );
                        echo Html::activeInput(
                            'text', $orgStructure, $regular,
                            ['class'       => 'part-field',
                             'placeholder' => $orgStructure->getAttributeLabel(
                                 $regular
                             )]
                        );
                        ?>
                        <span class="btn btn-danger remove-btn" id="remove_orgstruct_<?= $key ?>">
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                            </span>
                        </span>
                </div>
            <?php } ?>
        </div>
        <?= $form->field($orgStructure,'all_regular_count')->input('text',['class'=>'form-control']) ?>
        <?= $form->field($orgStructure,'informations_authority')->textarea(['rows'=>3]) ?>

        <!--    Оргструктура: рекоммендации    -->
        <div class="form-group field-orgstructure-informations-orgstruct_recommendations">
            <?php
            echo Html::activeLabel($orgStructure,'orgstruct_recommendations');
            echo Html::activeTextarea($orgStructure,'orgstruct_recommendations',['class'=>'form-control','id'=>'orgstr_recommendations']);
            ?>
            <span class="btn btn-danger form-control remove-btn" id="remove_orgstr_recommendations">
                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
            </span>
        </div>
    </div>
</div>