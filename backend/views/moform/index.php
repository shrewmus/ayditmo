<?php
/**
 * index.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/29/14
 * Time: 4:17 AM
 * Copyright 2014
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */

/** @var $municipality common\models\Municipality */
/** @var $municipalityCommon common\models\MunicipalityCommon */
/** @var $normativeLegal common\models\NormativeLegalSupport */
/** @var $orgStructure common\models\OrgStructure */
/** @var $mscopes array */

\backend\assets\MoformAsset::register($this);

$this->title = 'Создание: Муниципальное образование'

?>

    <div class="row">
        <h1><?= $this->title ?></h1>

        <?php $form= ActiveForm::begin(['id'=>'munFrm','options'=>['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($municipality,'municipality_name') ?>

        <?php //todo подгрузка изображения ?>

        <?= $form->field($municipality,'municipality_emblem')->fileInput(); ?>

        <?php
        /**
         * -----------------------
         *  1. Общие сведения
         * -----------------------
         */
        echo $this->render('_commonvals',['municipalityCommon'=>$municipalityCommon,'form'=>$form]);

        /**
         * -----------------------
         * 2. Нормативно правовое обеспечение
         * -----------------------
         */
        echo $this->render('_normativeLegal',['form'=>$form,'normativeLegal'=>$normativeLegal]);

        /**
         * ----------------------
         * 3. Организационно-штатная структура
         * ----------------------
         */
        echo $this->render('_orgStruct',['form'=>$form,'orgStructure' => $orgStructure]);

        ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>4. ИНФОРМАТИЗАЦИЯ СФЕР ДЕЯТЕЛЬНОСТИ</h2>
        </div>
        <div class="panel-body">
            <?php

            //todo вывод заполненных сфер (на будущее для редактирования)
            //todo underscore шаблоны
            ?>
            <?= $form->field($municipality, 'scope_prepdoc')->textarea(['rows' => 5]) ?>
            <span class="form-control btn btn-primary" style="margin: 5px 0" id="btnAddScope" data-toggle="modal" data-target="#scopeNameModal">Добавить сферу деятельности</span>
            <div class="panel-group" id="accordion" role="tablist">
            <?php
            /**
             * 4. Информатизация сфер деятельности
             */

            echo $this->render('_mscope',['form'=>$form,'municipality'=>$municipality,'mscopes'=>$mscopes]);
            ?>
            </div><!-- collapse id=accordion -->
        </div>
        <?php
        echo Html::submitButton('Сохранить', ['class' => 'form-control btn btn-success']);
        echo Html::a('Закрыть','/',['class'=>'btn btn-warning form-control']);
        echo "<div class='row' style='height: 34px'></div>";
        echo Html::a('Удалить','/site/modelete/'.$municipality->id,['class'=>'btn btn-danger form-control']);
        $form->end();
        ?>

    </div>
<!--     Модалка для названия добавляемой сферы       -->
        <div class="modal fade" id="scopeNameModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Название добавляемой сферы деятельности:</h5>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="newScopeName" class="form-control"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                        <button type="button" class="btn btn-primary" id="btnScopeProceed">Далее</button>
                    </div>
                </div>
            </div>
        </div>
<!--    Шаблоны для некоторых полей сферы    -->
        <!-- оргструктура сферы деятельности -->
        <script type="text/template" id="scopeOrgStat_tpl">
            <div class="form-group part-field">
                <input type="text" class="part-field" name="MunicipalityScope[<%= scopeIdx %>][org_str][<%= rowIdx %>][department]" placeholder="Отдел">
                <input type="text" class="part-field" name="MunicipalityScope[<%= scopeIdx %>][org_str][<%= rowIdx %>][fio]" placeholder="ФИО">
                <input type="text" class="part-field" name="MunicipalityScope[<%= scopeIdx %>][org_str][<%= rowIdx %>][count]" placeholder="Кол-во">
            </div>
        </script>

        <script type="text/template" id="scopeBaseFunc_tpl">
            <tr>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][bas_func][<%= rowIdx %>][taskName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][bas_func][<%= rowIdx %>][isysName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][bas_func][<%= rowIdx %>][isysCode]"></textarea></td>
            </tr>
        </script>

        <script type="text/template" id="scopeMunicipalService_tpl">
            <tr>
                <?php $scopeServ = new \common\components\sphereparts\ScopeService(); ?>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_serv][<%= rowIdx %>][serviceName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_serv][<%= rowIdx %>][regulationDetail]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_serv][<%= rowIdx %>][serviceAddress]"></textarea></td>
                <?php
                foreach(['servNum1993','isMFCService'] as $fldName) {
                    $out
                        = "<td><select name='MunicipalityScope[<%= scopeIdx %>][scope_serv][<%= rowIdx %>][$fldName]' class='form-control'>";
                    $opts = $scopeServ->getFieldPossibleValue($fldName);
                    foreach ($opts as $val => $text) {
                        $out .= "<option value='$val'>$text</option>";
                    }
                    $out .= "</select></td>";
                    echo $out;
                }
                ?>
            </tr>
        </script>
        <script type="text/template" id="scopeInfoObject_tpl">
            <tr>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_info][<%= rowIdx %>][objectName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_info][<%= rowIdx %>][sysName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_info][<%= rowIdx %>][responseDepartment]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][scope_info][<%= rowIdx %>][note]"></textarea></td>
            </tr>
        </script>
        <script type="text/template" id="scopeReports_tpl">
            <tr>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportName]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportInfo]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportConsumer]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportFormat]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportPeriod]"></textarea></td>
                <td><textarea class="form-control" name="MunicipalityScope[<%= scopeIdx %>][sc_reports][<%= rowIdx %>][reportBasis]"></textarea></td>
            </tr>
        </script>