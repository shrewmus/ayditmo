<?php
/**
 * Раздел общие сведения
 * _commonvals.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/7/15
 * Time: 1:33 AM
 * Copyright 2015
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>1. ОБЩИЕ СВЕДЕНИЯ</h2>
    </div>
    <div class="panel-body">
<?php

//модель \common\models\MunicipalityCommon

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */

$fAttr = $municipalityCommon->attributes();

foreach ($fAttr as $fieldId) {
    if (!in_array($fieldId, ['municipality_id', 'mcommon_id'])) {
        echo $form->field($municipalityCommon, $fieldId)->textarea(
            ['rows' => 2]
        );
    }
}

?>
    </div>
</div>