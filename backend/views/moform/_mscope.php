<?php
/**
 * _mscope.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/7/15
 * Time: 1:54 AM
 * Copyright 2015
 */

use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $municipalityScope common\models\MunicipalityScope */

    foreach ($mscopes as $i => $municipalityScope) {
        $scopeName = "MunicipalityScope[$i]";
        ?>
        <!--    панель для сферы деятельности    -->
        <div class="panel panel-default">
            <!--     заголовок (остается видимым)      -->
            <div class="panel-heading accordion-panel" id="scopeHeading_<?= $i ?>"
                 role="tab">
                <h3 class="panel-title">
                    <a href="#scopeCollapse_<?= $i ?>" data-parent="#accordion"
                       data-toggle="collapse"
                       aria-controls="scopeCollapse_<?= $i ?>" aria-expanded="true"><?= $municipalityScope->scope_name ?></a>
                    <input type="hidden" name="MunicipalityScope[<?= $i ?>][scope_name]" value="<?= $municipalityScope->scope_name ?>">
                    <span class="btn btn-danger" data-scopeid="<?= $i ?>" style="float: right;" id="remove_scopeHeading_<?= $i ?>">Удалить сферу</span>
                </h3>
            </div>
            <!--     контент для сферы (сворачиваемый)     -->
            <div id="scopeCollapse_<?= $i ?>" class="panel-collapse collapse in" aria-labelledby="scopeHeading_<?= $i ?>">
                <div class="panel-body" role="tabpanel">
                    <!--        куратор            -->
                    <div class="form-group field-municipalityscope-curator">
                        <?= Html::activeLabel( $municipalityScope, "[$i]curator" ); ?>
                        <?= Html::activeInput( 'text', $municipalityScope, "[$i]curator", ['class' => 'form-control', 'placeholder' => 'ФИО'] ); ?>
                    </div>
                    <!--        полномочия по развитию            -->
                    <div class="form-group field-municipalityscope-development_authority">
                        <?= Html::activeLabel( $municipalityScope, "[$i]development_authority" ); ?>
                        <?= Html::activeTextarea( $municipalityScope, "[$i]development_authority", ['class' => 'form-control', 'rows' => 4] ) ?>
                    </div>
                    <!--        штатная численность            -->
                    <div class="form-group field-municipalityscope-regular_count">
                        <?= Html::activeLabel( $municipalityScope, "[$i]regular_count" ) ?>
                        <?= Html::activeInput( 'text', $municipalityScope, "[$i]regular_count", ['class' => 'form-control', 'placeholder' => 'штатная численность'] ) ?>
                    </div>

                    <?php $mscopeParams = [
                        'i' => $i,
                        'municipalityScope' => $municipalityScope,
                        'scopeName' => $scopeName
                    ]; ?>

                    <!--        ===== ОРГСТРУКТУРА СФЕРЫ =====        -->

                    <?= $this->render('mscope/org_struct',$mscopeParams); ?>

                    <!--    информация по результатам интервью    -->
                    <div class="form-group field-municipalityscope-interview_info">
                        <?php
                        echo Html::activeLabel($municipalityScope, "[$i]interview_info");
                        echo Html::activeTextarea($municipalityScope, "[$i]interview_info", ['class' => 'form-control']);
                        ?>
                    </div>


                    <!--      =====  БАЗОВЫЕ ФУНИКЦИИ СФЕРЫ =====       -->

                    <?= $this->render('mscope/basic_functions',$mscopeParams) ?>

                    <!--  ===== Предоставляемы муниципальные услуги =====   -->

                    <?= $this->render('mscope/scope_services',$mscopeParams) ?>


                    <!--   ===== Объекты учета =====   -->

                    <?= $this->render('mscope/info_objects',$mscopeParams) ?>

                    <!-- ===== Предоставляемые отчетные документы: =====    -->

                    <?= $this->render('mscope/scope_reports',$mscopeParams) ?>

                    <!--  Заключение  -->

                    <div class="form-group field-municipalityscope-conclusion">
                        <?php
                        echo Html::activeLabel($municipalityScope,"[$i]conclusion");
                        echo Html::activeTextarea($municipalityScope,"[$i]conclusion",['class' => 'form-control','rows' => 5]);
                        ?>
                    </div>
                    <!--        рекомендации 4 раздел         -->
                    <div class="form-group field-orgstructure-informations-scope_recommendations">
                        <?php
                        echo Html::activeLabel($municipalityScope, "[$i]scope_recomendations");
                        echo Html::activeTextarea($municipalityScope, "[$i]scope_recomendations", ['class' => 'form-control','id'=>$i.'_scope_recommendations']);
                        ?>
                        <span class="btn btn-danger form-control remove-btn" id="remove_<?= $i ?>_scope_recommendations">
                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                        </span>
                    </div>
                </div><!-- end panel-body -->
            </div><!-- end panel-collapse (id scopeCollapse_N) -->
        </div><!-- end panel-default -->
    <?php } ?>

