<?php
/**
 * Поле редоставляемых муниципальных услуг
 * scope_services.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/8/15
 * Time: 4:16 PM
 * Copyright 2015
 * @var $municipalityScope \common\models\MunicipalityScope
 * @var $i integer
 * @var $scopeName string
 */
use common\components\sphereparts\ScopeService;
use yii\helpers\Html;

?>
<div class="form-group field-municipalityscope-municipal_services">
    <?= Html::activeLabel($municipalityScope, "[$i]scope_services") ?>
    <div class="panel panel-default" id="scope_municipal_service_<?= $i ?>">
        <div class="panel-body" >
            <table class="table">
                <tbody id="scopeMunicipalService_<?= $i ?>">
                    <tr>
                        <td>Наименование муниципальной услуги</td>
                        <td>Реквизиты административного регламента предоставления МУ (дата принятия)</td>
                        <td>Фактические адреса мест предоставления для каждой услуги, город, улица, дом, кабинет, кол-во раб.мест (за исключением МФЦ)</td>
                        <td>Предоставляется в электронном виде на РПГУ, этап по 1993-р</td>
                        <td>Предоставляются в МФЦ</td>
                    </tr>
                    <?php
                    $scopeServices = $municipalityScope->getScopeServices();
                    if(empty($scopeServices)){
                        $scopeServices = [new ScopeService()];
                    }
                    /**
                     * @var $scopeService \common\components\sphereparts\ScopeService
                     */
                    foreach($scopeServices as $s=>$scopeService){
                        $scServ = $scopeName."[scope_serv][$s]";
                        ?>
                        <tr>
                            <td><?= Html::textarea($scServ."[serviceName]", $scopeService->getField('serviceName'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scServ."[regulationDetail]", $scopeService->getField('regulationDetail'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scServ."[serviceAddress]", $scopeService->getField('serviceAddress'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::dropDownList($scServ."[servNum1993]", $scopeService->getField('servNum1993'),
                                    $scopeService->getFieldPossibleValue('servNum1993'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::dropDownList( $scServ."[isMFCService]", $scopeService->getField('isMFCService'),
                                    $scopeService->getFieldPossibleValue('isMFCService'), ['class' => 'form-control']) ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <span class="btn btn-success add-btn" id="add_scopeMunicipalService_<?= $i ?>">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </span>
        </div>
        <span class="btn btn-danger form-control remove-btn" id="remove_scope_municipal_service_<?= $i ?>" style="margin-top: -55px">
            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
        </span>
    </div>
</div>