<?php
/**
 * поле Базовые функции сферы
 * basic_functions.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/8/15
 * Time: 4:06 PM
 * Copyright 2015
 * @var $municipalityScope \common\models\MunicipalityScope
 * @var $i integer
 * @var $scopeName string
 */
use common\components\sphereparts\BasicFunction;
use yii\helpers\Html;

?>
<div class="form-group field-municipalityscope-basic_functions">

    <?= Html::activeLabel( $municipalityScope, "[$i]basic_functions" ) ?>

    <div class="panel panel-default" id="scope_basic_func_<?= $i ?>">
        <div class="panel-body">
            <table  class="table">
                <tbody id="scopeBaseFunc_<?= $i ?>">
                    <tr>
                        <td>Наименование полномочий/функций (задачи)</td>
                        <td>Название ИС/ПО используемых для автоматизации полномочий/функций</td>
                        <td>Код ИС по классификатору (приложение 0)</td>
                    </tr>
                    <?php

                    $basicFunctions = $municipalityScope->getBasicFunctions();
                    if(empty($basicFunctions)){
                        $basicFunctions = [new BasicFunction()];
                    }

                    /**
                     * @var  $basicFunc \common\components\sphereparts\BasicFunction
                     */
                    foreach($basicFunctions as $s => $basicFunc){
                        $bfunName = $scopeName."[bas_func][$s]";
                        //поля базовых функций
                        ?>
                        <tr>
                            <td><?= Html::textarea($bfunName."[taskName]", $basicFunc->getField('taskName'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($bfunName."[isysName]", $basicFunc->getField('isysName'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($bfunName."[isysCode]", $basicFunc->getField('isysCode'), ['class' => 'form-control']) ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <span class="btn btn-success add-btn" id="add_scopeBaseFunc_<?= $i ?>">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </span>
        </div>
        <span class="btn btn-danger form-control remove-btn" id="remove_scope_basic_func_<?= $i ?>" style="margin-top: -55px">
            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
        </span>
    </div>
</div>