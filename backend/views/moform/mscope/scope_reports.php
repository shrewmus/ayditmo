<?php
/**
 * Поле Предоставляемые отчетные документы
 * scope_reporst.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/8/15
 * Time: 4:36 PM
 * Copyright 2015
 * @var $municipalityScope \common\models\MunicipalityScope
 * @var $i integer
 * @var $scopeName string
 */
use common\components\sphereparts\ScopeReport;
use yii\helpers\Html;

?>

<div class="form-group field-municipalityscope-reports">
    <?= Html::activeLabel($municipalityScope, "[$i]scope_reports") ?>
    <div class="panel panel-default" id="scope_reports_<?= $i ?>">
        <div class="panel-body" >
            <?php

            //todo сделать имена полей для массива

            ?>
            <table class="table">
                <tbody id="scopeReports_<?= $i ?>">
                    <tr>
                        <td>Название отчета</td>
                        <td>Краткие сведения о составе данных предоставляемых в отчете</td>
                        <td>Кому предоставляется, название РОИВ/ФОИВ</td>
                        <td>Форма предоставления, ИС, по форме, в бумажном виде</td>
                        <td>Периодичность предоставления отчета, месяц, квартал, год, по запросу</td>
                        <td>Основание для предоставления отчета, № ФЗ, постановление/распоряжение, письмо и т.д.</td>
                    </tr>
                    <?php
                    $reports = $municipalityScope->getScopeReports();
                    if(empty($reports)){
                        $reports = [new ScopeReport()];
                    }
                    /**
                     * @var $report \common\components\sphereparts\ScopeReport
                     */
                    foreach($reports as $s=>$report){
                        $scRep = $scopeName."[sc_reports][$s]";
                        ?>
                        <tr>
                            <td><?= Html::textarea($scRep."[reportName]", $report->getField('reportName'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scRep."[reportInfo]", $report->getField('reportInfo'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scRep."[reportConsumer]", $report->getField('reportConsumer'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scRep."[reportFormat]", $report->getField('reportFormat'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scRep."[reportPeriod]", $report->getField('reportPeriod'), ['class' => 'form-control']) ?></td>
                            <td><?= Html::textarea($scRep."[reportBasis]", $report->getField('reportBasis'), ['class' => 'form-control']) ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <span class="btn btn-success add-btn" id="add_scopeReports_<?= $i ?>">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </span>
        </div>
        <span class="btn btn-danger form-control remove-btn" id="remove_scope_reports_<?= $i ?>" style="margin-top: -55px">
            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
        </span>
    </div>
</div>