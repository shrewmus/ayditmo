<?php
/**
 * Поле Объекты учета
 * info_objects.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/8/15
 * Time: 4:28 PM
 * Copyright 2015
 * @var $municipalityScope \common\models\MunicipalityScope
 * @var $i integer
 * @var $scopeName string
 */
use common\components\sphereparts\InfoObject;
use yii\helpers\Html;

?>

<div class="form-group field-municipalityscope-info_objects">
    <?= Html::activeLabel($municipalityScope, "[$i]info_objects") ?>
    <div class="panel panel-default" id="scope_info_obj_<?= $i ?>">
        <div class="panel-body" >
            <table class="table">
                <tbody id="scopeInfoObject_<?= $i ?>">
                <tr>
                    <td>Название объекта учета/данные</td>
                    <td>В какой системе ведется реестр/СУБД</td>
                    <td>Подразделение ответственное за актуальность данных (владелец данных, в том числе РОИВ, ФОИВ и т.д.)</td>
                    <td>Примечание</td>
                </tr>
                <?php
                $infoObjects = $municipalityScope->getInfoObjects();
                if(empty($infoObjects)){
                    $infoObjects = [new InfoObject()];
                }
                /**
                 * @var $infoObject \common\components\sphereparts\InfoObject
                 */
                foreach($infoObjects as $s => $infoObject){
                    $infObj = $scopeName."[scope_info][$s]";
                    ?>
                    <tr>
                        <td><?= Html::textarea($infObj."[objectName]", $infoObject->getField('objectName'), ['class' => 'form-control']) ?></td>
                        <td><?= Html::textarea($infObj."[sysName]", $infoObject->getField('sysName'), ['class' => 'form-control']) ?></td>
                        <td><?= Html::textarea($infObj."[responseDepartment]", $infoObject->getField('responseDepartment'), ['class' => 'form-control']) ?></td>
                        <td><?= Html::textarea($infObj."[note]", $infoObject->getField('note'), ['class' => 'form-control']) ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <span class="btn btn-success add-btn" id="add_scopeInfoObject_<?= $i ?>">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </span>
        </div>
        <span class="btn btn-danger form-control remove-btn" id="remove_scope_info_obj_<?= $i ?>" style="margin-top: -55px">
            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
        </span>
    </div>
</div>