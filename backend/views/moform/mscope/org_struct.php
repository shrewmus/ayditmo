<?php
/**
 * поле Оргструктура сферы деяетльности
 * org_struct.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/8/15
 * Time: 3:58 PM
 * Copyright 2015
 * @var $municipalityScope \common\models\MunicipalityScope
 * @var $i integer
 * @var $scopeName string
 */

use common\components\sphereparts\OrgStructure;
use yii\helpers\Html;
?>
<div class="form-group field-municipalityscope-org_structure">
    <?= Html::activeLabel($municipalityScope, "[$i]org_structure") ?>
    <div class="panel panel-default">
        <div class="panel-body" id="scope_org_structure_<?= $i ?>">
            <div class="row" id="scopeOrgStat_<?= $i ?>">
                <?php

                $orgStructures = $municipalityScope->getOrgStructures();
                if (empty($orgStructures)) {
                    $orgStructures = [new OrgStructure()];
                }

                /** @var $scopeOrgStr \common\components\sphereparts\OrgStructure */
                foreach ($orgStructures as $s => $scopeOrgStr) { ?>
                    <div class="form-group part-field">
                        <?php
                        $orgName = $scopeName."[org_str][$s]";
                        // поля оргструктуры: отдел, фио, кол-во
                        echo Html::input('text', $orgName."[department]", $scopeOrgStr->getField('department'), ['class' => 'part-field', 'placeholder' => 'Отдел']);
                        echo Html::input('text', $orgName."[fio]", $scopeOrgStr->getField('fio'), ['class' => 'part-field', 'placeholder' => 'ФИО']);
                        echo Html::input('text', $orgName."[count]", $scopeOrgStr->getField('count'), ['class' => 'part-field', 'placeholder' => 'Кол-во']);
                        ?>
                    </div>
                <?php } ?>
            </div>
            <span class="btn btn-success add-btn" id="add_scopeOrgStat_<?= $i ?>">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </span>
        </div>
    </div>
</div>
