<?php
/**
 * Список МО для лобби
 * _molist.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/10/15
 * Time: 3:05 AM
 * Copyright 2015
 */

/** @var $this \yii\web\View */

?>
<div class="container site-login">
    <?php if (empty($mos)) { //если ни одного МО нет - выводится строка с 4 тайлами - заглушками?>
        <div class="row">
            <?php for ($i = 0; $i < 4; $i++) { ?>
                <div class="col-md-3">
                    <?= $this->render('_emptyMOTile') ?>
                </div>
            <?php } ?>
        </div>
    <?php
    } else {

        $cntr = 0;

        /** @var \common\components\front\MOTile $mo */
        foreach ($mos as $mo) {//выводится по 4 МО в строку
            if ($cntr == 0) {
                echo '<div class="row">';
            }

            echo '<div class="col-md-3">'.$this->render('_MOTile', ['mo' => $mo]).'</div>';
            $cntr++;
            if ($cntr == 4) {
                $cntr = 0;
                echo "</div>";
            }
        }
        if ($cntr > 0) {//если вывелось в последней строке меньше 4-х - добивается заглушками
            for ($i = $cntr; $i < 4; $i++) {
                echo '<div class="col-md-3">'.$this->render('_emptyMOTile').'</div>';
            }
        }
    } ?>

</div>