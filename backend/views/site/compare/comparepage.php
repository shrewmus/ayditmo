<?php
/**
 * comparepage.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 01/13/15
 * Time: 4:36 PM
 * Copyright 2015
 */

use yii\helpers\Html;

/** @var $this \yii\web\View */

\backend\assets\CompareAsset::register($this);

function emtd($cnt){
    $emTD = "";
    for($s = 0; $s < $cnt; $s++){
        $emTD.=tdf("");
    }
    return $emTD;
}

$names = $muns[0]->getNames();

/**
 * Оборачивалка в td
 * @param $data string
 *
 * @return string
 */
function tdf($data){
    return "<td>$data</td>";
}

/**
 * Обработка сложных полей сферы деяетельности
 * @param $cell \common\components\sphereparts\SphereSubrow
 * @param $fieldKey string
 *
 * @return array
 */
function scopeSpecFields($cell,$fieldKey){
    $cnames = '';
    $cells = '';
    $specFields = [
        'org_structure'=>['department','fio','count'],
        'basic_functions'=>['taskName','isysName','isysCode'],
        'scope_services'=>['serviceName','regulationDetail','serviceAddress','servNum1993','isMFCService'],
        'info_objects'=>['objectName','sysName','responseDepartment','note'],
        'scope_reports'=>['reportName','reportInfo','reportConsumer','reportFormat','reportPeriod','reportBasis']
    ];
    $labels = $cell->getLabels();
    foreach($specFields[$fieldKey] as $fld){
        $cnames .= tdf($labels[$fld]);
        if($fieldKey == 'scope_services' and in_array($fld,['servNum1993','isMFCService'])){
            $cells.= tdf($cell->getFieldPossibleValue($fld)[$cell->getField($fld)]);
        }else{
            $cells.= tdf($cell->getField($fld));
        }
    }
    return ['cnames'=>$cnames,'cells'=>$cells];
}
//разделение общих полей и полей сфер деятельности
$scopeNames = $names[4];
$names[4] = ['scope_prepdoc'=>$scopeNames['scope_prepdoc']];
unset($scopeNames['scope_prepdoc']);

$munsCnt = count($muns) + 1;//количество отобранных для сравнения МО + столбец названий
$partTitles = [];//заголовки разделов
$rows = [];//поля
$scopeRows=[];//поля сфер

$maxScopes = 0;//максимальное число сфер деятельности у МО
//по выбранным муниципалитетам
/** @var \common\models\Municipality $municipality */
foreach($muns as $mIDx=>$municipality){

    $scopCnt = count($municipality->municipalityScopes);
    if( $scopCnt > $maxScopes){
        $maxScopes = $scopCnt;
    }

    $parts = $municipality->getParts();//массив полей по разделам
    //отдельные поля
    $rows[$mIDx+1]['municipality_name'] = "<th>".$municipality->municipality_name."</th>";
    $munImg = ($municipality->municipality_emblem) ? $municipality->municipality_emblem : Yii::$app->params('defaultMOImage');
    $rows[$mIDx+1]['municipality_emblem'] = tdf("<img src='/uploads/{$munImg}'>");

    //пошли по разделам
    foreach($parts as $partKey => $part){
        $partTitles[$partKey] = $part['partName'];

        if($part['key'] != 'scopes'){//для разделов - не сферы
            foreach($part['partRows'] as $pKey => $partRow){
                if(! is_array($partRow['value'])){
                    $rows[$mIDx+1][$pKey] = tdf($partRow['value']);//простое поле
                }else{//несколько полей оргструктуры
                    $cnames = "";
                    $cells = "";

                    foreach ($partRow['value'] as $cellIdx => $cell) {
                        $cnames .= tdf($cell['name']);
                        $cells .= tdf("<div>{$cell['value']}</div>");
                    }
                    unset($names[$partKey+1][$partRow['key']."_regular"]);
                    unset($names[$partKey+1][$partRow['key']."_count"]);
                    $names[$partKey+1][$partRow['key']] = $partRow['name'];

                    //в результате подтаблица (в ячейке)
                    $rows[$mIDx+1][$partRow['key']] = tdf("<table class='table table-condensed'><tbody><tr>{$cnames}</tr><tr>{$cells}</tr></tbody></table>");
                }
            }
        }else{//тут выбираем сферы
            $rows[$mIDx+1]['scope_prepdoc'] = tdf($municipality->scope_prepdoc);//одно поле общее для 4-го раздела
            $scopeRows[$mIDx+1] = [];//по колонкам
            foreach($part['partRows'] as $scopeIdx => $scope){//в колонке может быть несколько сфер
                foreach($scope as $scFld => $scFldVal){
                    if(is_array($scFldVal['value'])){//сложные поля сферы
                        foreach($scFldVal['value'] as $cellIdx=>$cell){
                            $t = 1;
                            $cellLabels = $cell->getLabels();
                            //todo нужно упростить перебором полей
                            $tblVars = scopeSpecFields($cell,$scFldVal['key']);
                        }
                        //подтаблица в колонке и для отдельной сферы
                        $scopeRows[$mIDx+1][$scopeIdx][$scFld] = "<table class='table table-condensed table-bordered'><tbody><tr>{$tblVars['cnames']}</tr><tr>{$tblVars['cells']}</tr></tbody></table>";
                    }else{
                        //простая строка в колонке и для отдельной сферы
                        $scopeRows[$mIDx+1][$scopeIdx][$scFld]=$scFldVal['value'];
                    }
                }//сложные поля
            }
        }//обработка полей сферы
    }
}

//строки основных разделов, готовые к вставке в таблицу
$strRows = [];

array_unshift($partTitles,'');//вставка раздела без названия - пара полей вне разделов
// из массивов делаем строки таблиц обычных разделов
for ($k = 0; $k <= 4; $k++) {
    //строка для заголовка раздела
    if($k != 0){
        $strRows[] = "<td colspan='$munsCnt'>" . $partTitles[$k] . "</td>";
    }
    foreach ($names[$k] as $nameKey => $nameField) {
        if($k == 0 && $nameKey == 'municipality_name'){
            $row[0] = "<th>$nameField</th>";
        }else{
            $row[0] = tdf($nameField);//первая колонка - имя
        }
        foreach ($rows as $col => $column) {//остальные колонки значения
            $row[$col + 1] = $column[$nameKey];
        }
        $strRows[] = join("", $row);
    }
    $row = [];
}

// строки таблиц отдельно сферы
$strScopeRows = [];

//количество сворачивающихся строк = максимальному количеству сфер деятельности среди выбранных МО
for($sIdx = 0; $sIdx < $maxScopes; $sIdx++){//фактически цикл по горизонтальным блокам из строк
    //строка таблицы для сворачивания
    $emtd = emtd($munsCnt -1);
    $strScopeRows[] = "<tr data-toggle='collapse' data-target='.{$sIdx}collapse'><td colspan='$munsCnt'><span class='btn btn-default'><strong>Сфера деятельности (свернуть/развернуть)</strong></span></td> </tr>";

    foreach($scopeNames as $sIDx => $sName){//в каждом блоке строк столько сколько полей-имен для сферы
        $row[0] = tdf($sName);//row - это строка, состоящая из ячеек, первая для названий
        for($i = 1; $i < $munsCnt; $i++){//остальные ячейки по выбранным МО
            if(isset($scopeRows[$i][$sIdx])){//если есть для найденного МО данные
                $row[$i] = tdf($scopeRows[$i][$sIdx][$sIDx]);
            }else{//иначе
                $row[$i] = tdf("");
            }
        }
        //ячейки соединяются в строку с классом для сворачивалки
        $strScopeRows[]="<tr class='collapse in {$sIdx}collapse'>".join("",$row)."</tr>";
//        $strScopeRows[]="<tr >".join("",$row)."</tr>";
    }
}

?>

<div>
    <div class="scroll-pane component" style="margin-top: 80px">
        <a href="/" class="btn-warning btn">Закрыть</a>
        <table class="compare-tbl cmp-column table-hover table-bordered table-striped" id="comp-head">
            <thead>
                <?= "<tr>".$strRows[0]."</tr>"; ?>

            </thead>
        </table>
        <table class="compare-tbl cmp-column table-hover table-bordered table-striped" id="comp-body">
            <tbody>
            <?php
            $row[0]=tdf("");
            for($k = 1; $k < $munsCnt; $k++){

                $row[$k] = "<td><span class='btn btn-danger compare-remove' data-munid='{$muns[$k-1]->id}' data-ids='".Yii::$app->request->get('ids')."'>Удалить</span></td>";
            }
            $strRows[0] = "<tr class='hdr-row'>".join("",$row)."</tr>";
            foreach ($strRows as $rowNum => $strRow) {//вывод обычных полей
                if($rowNum == 1){
                    echo "<tr class='hdr-row'>$strRow</tr>";
                }else{
                    echo "<tr>$strRow</tr>";
                }
            }
            foreach($strScopeRows as $scRow){//вывод строк для сфер
                echo $scRow;
            }
            ?>
            </tbody>
        </table>
        <a href="/" class="btn btn-warning">Закрыть</a>
    </div>
</div>

