<?php
/**
 * Страница просмотра МО
 * mopage.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/11/15
 * Time: 2:49 AM
 * Copyright 2015
 */
use yii\helpers\Html;


/** @var $this yii\web\View */
/** @var $municipality \common\models\Municipality */

$this->title = $title;
$imgFolder = '/uploads/';
$emblem = ($municipality->municipality_emblem)? $imgFolder.$municipality->municipality_emblem : Yii::$app->params['defaultMOImage'];
?>
<style>
.row {
	padding: 15px 0;
}

.panel-body {
	padding: 0 15px;
}

.panel-body .row:nth-child(even) {
	background-color: #F7F7F7;
}

.viewfield .form-control {
	height: auto!important;
	overflow: visible!important;
	background: none!important;
	border: 0 none!important;
	box-shadow: none!important;
	padding:  6px 0;
	max-height: 10000px!important;
}

.small-col{
	/*width: 55px !important;*/
	padding: 0;
	/*font-size: 8px;*/
}

th div.small-col{
	/*width: 255px !important;*/
	/*transform: rotate(-90.0deg) !important;*/
	/*overflow: visible !important;*/
}

.viewfield table th div, .viewfield table td div{
	height: auto;
	min-height: 34px;
	max-height: 100px ;
	overflow-y: auto ;
}

.panel .panel{
	border: none;
}

</style>

<div class="row text-center">
    <div class="col-md-12" style="margin: 15px 0;">
        <img src="<?= $emblem ?>" style="float:left; margin-right: 10px; width: 68px; height: auto;" />
        <h1 style="float: left;"><?= $this->title ?></h1>
    </div>
</div>

<!--  -->
<?php
foreach($parts as $part){
    echo $this->render('_partPanels',$part);
}
?>
<a href="/" class="btn btn-warning">Закрыть</a>
