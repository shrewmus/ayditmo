<?php
/**
 * Поля сферы деятельности МО
 * _scopes.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/11/15
 * Time: 6:46 PM
 * Copyright 2015
 */

/** @var $this \yii\web\View */
?>
<div class="panel-group" id="accordion" role="tablist">
    <div class="panel-heading accordion-panel" id="scopeHeading_<?= $i ?>" role="tab">
        <h3 class="panel-title">
            <a href="#scopeCollapse_<?= $i ?>"><?= $scope['name'] ?></a>
        </h3>
    </div>
    <div id="scopeCollapse_<?= $i ?>" class="panel-collapse collapse in" aria-labelledby="scopeHeading_<?= $i ?>">
        <div class="panel-body" role="tabpanel">
            <?php
            $filterKeys = ['org_structure','basic_functions','scope_services','info_objects','scope_reports'];
            foreach($scope['value'] as $fld){
                if(in_array($fld['key'],$filterKeys)){

                }else{
                    echo $this->render('_paramPairRow',$fld);
                }
            }
            ?>
        </div>
    </div>
</div>