<?php
/**
 * Вывод полей по разделом
 * Каждый раздел в отдельной панели
 * _partPanels.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/11/15
 * Time: 5:18 AM
 * Copyright 2015
 */
?>
<div class="panel panel-default">
    <div class="panel-heading"><h2><?= $partName ?></h2></div>
    <div class="panel-body">
        <?php
        foreach($partRows as $row){

            if(isset($row['value']) && (! is_array($row['value']))){
                echo $this->render('_paramPairRow',$row);
            }else{
                if(isset($row['value']) && is_null($row['value']) ){
                    echo $this->render('_paramPairRow',$row);
                }else{

                    switch ($key) {
                        case 'orgstr':
                            //вывод составных полей оргструктуры
                            $resValue = '';
                            //соединение значений в одну строку
                            if(is_array($row['value'])){
                                foreach ($row['value'] as $subRow) {
                                    $resValue .= $subRow['name'] . ': ' . $subRow['value'] . ';&nbsp;';
                                }
                            }
                            $row['value'] = $resValue;
                            echo $this->render('_paramPairRow', $row);
                            break;
                        case 'scopes'://вывод множественных полей сферы
                            echo '<div class="panel panel-default">';
                            //todo объединение нескольких таких полей
                            $specFields = [
                                'org_structure'=>['department','fio','count'],
                                'basic_functions'=>['taskName','isysName','isysCode'],
                                'scope_services'=>['serviceName','regulationDetail','serviceAddress','servNum1993','isMFCService'],
                                'info_objects'=>['objectName','sysName','responseDepartment','note'],
                                'scope_reports'=>['reportName','reportInfo','reportConsumer','reportFormat','reportPeriod','reportBasis']
                            ];
                            foreach ($row as $rowKey=>$subRow) {
                                if (!is_array($subRow['value'])) {
                                    echo $this->render('_paramPairRow', $subRow);
                                } else {
                                    $sRow['name'] = $subRow['name'];
                                    $sRow['value'] = '';
                                    if(is_array($subRow['value']) && (count($subRow['value']) > 0)) {
                                        $subRowLabels = $subRow['value'][0]->getLabels();
                                        $sRow['value'] = "<table class='table'><thead><tr>";
                                        foreach ($subRowLabels as $kidx=>$th) {
                                            if(($subRow['key'] == 'scope_services')and(in_array($kidx,['servNum1993','isMFCService']))){

                                                $sRow['value'] .= "<th><div class='small-col'>$th</div></th>";
                                            }else{
                                                $sRow['value'] .= "<th>$th</th>";
                                            }
                                        }
                                        $sRow['value'] .= "</thead><tbody>";


                                        /** @var \common\components\sphereparts\SphereSubrow $scopeComponent */
                                        foreach($subRow['value'] as $scopeComponent){
                                            $t = 0;
                                            $sRow['value'].="<tr>";
                                            foreach($specFields[$subRow['key']] as $fld){
                                                if($subRow['key'] == 'scope_services' and in_array($fld,['servNum1993','isMFCService'])){
                                                    $sRow['value'].= "<td><div class='small-col'>".$scopeComponent->getFieldPossibleValue($fld)[$scopeComponent->getField($fld)]."</div></td>";
                                                }else{
                                                    $sRow['value'].= "<td><div>".$scopeComponent->getField($fld)."</div></td>";
                                                }
                                            }
                                            $sRow['value'].="</tr>";
                                        }

                                       

                                    }
                                    $sRow['value'] .= "</tbody></table>";
                                    $sRow['isTable'] = true;
                                    echo $this->render('_paramPairRow',$sRow);
                                }
                            }
                            echo '</div>';
                            break;
                    }//end switch
                }

            }
        }
        ?>
    </div>
</div>