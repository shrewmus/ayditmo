<?php
/**
 * Простой блок из подписи к полю и значению поля
 * _paramPairRow.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/11/15
 * Time: 4:33 AM
 * Copyright 2015
 */
?>
<div class="row">
    <div class="col-md-12">
        <strong><?= $name ?>:</strong>
    </div>
    <div class="col-md-12 viewfield">
        <?php if(isset($isTable)){
            echo $value;
        }else{?>
        <p class="form-control"><?= $value ?></p>
        <?php } ?>
    </div>
</div>