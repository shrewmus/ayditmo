<?php

// Вывод лобби с формой авторизации

use common\models\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

\backend\assets\LobbyAsset::register($this);

$this->title = 'Вход';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container site-login">

    <?= $this->render('_lobbyHead') ?>

    <div class="row">
        <div class="col-md-4 col-centered">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' =>[
                    'template' => '{input}'
                ]
            ]); ?>
                <?= $form->field($model, 'username')->textInput(['placeholder'=>'Логин']) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Пароль']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?= $this->render('_molist',$indexParams);

