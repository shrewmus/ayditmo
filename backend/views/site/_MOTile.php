<?php
/**
 * Тайл МО с сылкой на просмотр
 * _MOTile.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/10/15
 * Time: 3:26 AM
 * Copyright 2015
 */

/** @var $mo \common\components\front\MOTile */
?>
<div class="motile">
    <a href="<?= $mo->getLink() ?>" class="municipality-front">
        <div class="motile-name"><?= $mo->getName() ?>
            <span class="label label-success" style="display: none;" title="Сравнить">
                <span class="glyphicon glyphicon-check"></span>
            </span>
        </div>
        <div>
            <?php
            //если нет загруженного изображения - используется дефолтное
            $img = $mo->getImg();
            if (!$img) {
                $img = Yii::$app->params['defaultMOImage'];
            }
            ?>
            <img src="<?= $img ?>"/>
        </div>
        <div><?= $mo->getDate() ?></div>

    </a>

    <div class="compare-controls" data-moid="<?= $mo->getId() ?>">
        <div class="compare-body" style="display: none">
            <span class="label label-primary add-compare">к сравнению</span>
        </div>
    </div>
</div>