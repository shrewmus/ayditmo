<?php
/* @var $this yii\web\View */

// Вывод лобби

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\backend\assets\LobbyAsset::register($this);

$this->title = 'Информатизация МО Московской области';
/** @var $button string */
?>
<div class="container site-login">

    <?= $this->render('_lobbyHead') ?>
    <?= $button; ?>

</div>
<?= $this->render('_molist',$indexParams) ?>

