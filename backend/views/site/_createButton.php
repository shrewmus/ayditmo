<?php
/**
 * Кнопка с сылкой создания нового МО
 * _createButton.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/10/15
 * Time: 3:08 AM
 * Copyright 2015
 */
?>
<div class="row">
    <div class="col-md-4 col-centered" style="padding: 140px 0;">
        <a href="/moform/create" class="btn btn-primary">Добавить Муниципальное образование</a>
    </div>
</div>