<?php
/**
 * _lobbyHead.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.01.15
 * Time: 16:01
 * Copyright 2015
 */

?>
<div class="row lobby-head"><img src="/img/u0_normal.png"/>
    <h1>Исследования состояния информатизации<br/>
        Муниципальных образований<br/>
        Московской области
    </h1>
</div>