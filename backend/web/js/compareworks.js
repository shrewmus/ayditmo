/**
 * compareworks.js
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 15.01.15
 * Time: 11:40
 * Copyright 2015
 */

$(document).ready(function () {

    //var whei = $(window).height();
    //console.log(whei);

    //$.fn.dataTableExt.sErrMode = 'throw';

   /* var table = $('#compare-tbl').DataTable( {
        "scrollY": whei+"px",
        "scrollX": "100%",
        //"scrollCollapse": true,
        "bSort" : false,
        "paging": false,
        "searching": false,
        "sDom": '<"top">rt<"bottom"flp><"clear">'
    } );*/
    //$.fn.dataTableExt.sErrMode = 'throw';
    //new $.fn.dataTable.FixedColumns(table,{leftColumns: 1});


    //var colsCnt = $('#comp-body .hdr-row').get(0).cells.length;
    var colheads = [];
    //colheads = Array.apply(null,Array(colsCnt));
    //colheads = colheads.map(function(){return 0});

    /**
     * Поправка ширины "заголовка"
     */
    function checkColWidth(){
        //колонки из строки основной таблицы
        var cols = $($('#comp-body tr.hdr-row').get(0)).find('td');
        cols.each(function (idx, elem) {
            //запоминается ширина колонок
            colheads[idx] = $(elem).width();
            if (idx == cols.length - 1) {//если прошли все ячейки
                $('#comp-head th').each(function (idx, elem) {
                    //уравнивается ширина таблиц заголовка и основной
                    $("#comp-head").width($("#comp-body").width() + 'px');
                    var colWidth = colheads[idx] + 12;//добавляется паддинги (todo вычислять паддинги)
                    $(elem).css('width', colWidth + 'px');
                });
            }
        });
    }

    checkColWidth();


    //изначальное положение по высоте заголовочной таблицы
    var def_offset = $('#comp-body').offset().top;

    /**
     * При скроле страницы делается "прилипание"
     */
    $(window).scroll(function () {
        var headTbl = $('#comp-head');

        //поправка смещения при изменении css position
        function changeOffset() {
            //заголовочная равняется по основной
            headTbl.addClass('stick-top');
            var offset = headTbl.offset();
            offset.left = $('#comp-body').offset().left;
            headTbl.offset(offset);
        }
        if($(this).scrollTop() > def_offset){//если "уплыла" - "прилепляется" к верху
            headTbl.addClass('stick-top');
            changeOffset();
        }else{
            $('#comp-head').removeClass('stick-top');//иначе - ставится на место
        }
    });

    /**
     * При удалении столбца - редирект с массивом id без удаленного
     */
    $('.compare-remove').each(function (idx, elem) {
       $(elem).click(function(){
           var idm = $(this).data('munid');
           var ids = $(this).data('ids');
           var arid = ids.indexOf(idm);
           ids.splice(arid,1);
           if(ids.length > 0){
               ids = JSON.stringify(ids);
               window.location.search = 'ids='+ids;
           }

       })
    });


});