/**
 * lobbyworks
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/13/15
 * Time: 11:07 AM
 * Copyright 2015
 */

$(document).ready(function () {

    var toCompareArray = [];

    function getCompareUrl(){
        var serialized = JSON.stringify(toCompareArray);
        var url = '/site/compare?ids=' + encodeURIComponent(serialized);
        return url;
    }

    $('.motile-name .label').each(function (idx, elem) {
        $(elem).click(function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            window.location.href = getCompareUrl();
        });
    });

    $('.compare-body .add-compare').click(function () {
        
        var moid = $(this).closest('.compare-controls').data('moid');
        var moidPosition = toCompareArray.indexOf(moid);
        
        if( moidPosition >= 0){
            toCompareArray.splice(moidPosition,1);
            if(toCompareArray.length > 0){
                $('#btnCompare').attr('href',getCompareUrl());
            }else{
                $('#btnCompare').remove();
            }
            $(this).text('к сравнению');
            $(this).addClass('btn-primary');
            $(this).removeClass('btn-success');
            $(this).closest('.motile').find('.motile-name .label').css('display','none');
        }else{
            if(toCompareArray.length == 0){
                $('.lobby-head').parent().append($('<a id="btnCompare" class="btn btn-danger">Сравнить</a>'));
            }
            toCompareArray.push(moid);
            $('#btnCompare').attr('href',getCompareUrl());

            $(this).text('из сравнения');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $(this).closest('.motile').find('.motile-name .label').css('display','inline-block');
        }
        
    });

    
    
    $('.compare-controls').parent().hover(
        function(){
            $(this).find('.compare-body').fadeIn(200);
        },

        function(){
            $(this).find('.compare-body').fadeOut(200);
        }
    );

});
