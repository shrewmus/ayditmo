/**
 * formworks
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/7/15
 * Time: 1:22 AM
 * Copyright 2015
 */

var Shr = Shr || {};

$(document).ready(function(){


    /**
     * Добавление пустой сферы деятельности
     * Обработка клика "далее" в модалке
     */
    $('#btnScopeProceed').on('click',function(){

        var scopeName = $("#newScopeName").val();
        //если есть название сферы деятельности
        if(scopeName.trim()){
            var scopes =  $('[id ^= "scopeHeading_"]');
            var maxScopeId = 0;
            //получаем максимальный номер из существующих в форме чтобы не было дублирования id
            _.each(scopes,function(scope,idx){
                var scopeId = $(scope).attr('id');
                scopeId = scopeId.split('_');
                scopeId = scopeId[1];
                if(parseInt(scopeId) > maxScopeId){
                    maxScopeId = parseInt(scopeId);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/moform/emptyscope',
                data:{cnt:parseInt(maxScopeId) + 1,newName:scopeName},
                dataType:'html',
                success: function (data) {
                    var dat = $(data);
                    parseRemoveReccomend(dat);
                    parseAddTpl(dat);
                    $('#accordion').append(dat);
                }
            });
        }

        $('#newScopeName').val('');
        $('#scopeNameModal').modal('hide');

    });

    function parseRemoveReccomend(parent){
        var removes = $(parent).find('[id ^= "remove"]');
        _.each(removes, function (remBtn) {
            var id = remBtn.id.split('_');
            id.shift();
            id = id.join('_');
            $(remBtn).on('click', function () {
                $('#'+id).parent().remove();
                this.remove();
            })

        })
    }

    parseRemoveReccomend($('#munFrm'));
    parseAddTpl($('[id ^= "scopeCollapse_"]'));

    function parseAddTpl(parent){
        var addbtns = $(parent).find('[id ^= "add"]');
        _.each(addbtns,function(addBtn){
            var id = addBtn.id.split('_');
            var tplParams = {};
            id.shift();
            var tplId = id[0]+"_tpl";
            tplParams.scopeIdx = id[1];
            id = id.join('_');
            $(addBtn).on('click', function () {

                var container = $('#'+id);
                tplParams.rowIdx = container.children().length;
                var tpl = _.template($('#'+tplId).html());
                container.append(tpl(tplParams));
            })
        });
    }


});