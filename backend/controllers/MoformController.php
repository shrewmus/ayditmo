<?php
/**
 * MoformController.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 7:39 PM
 * Copyright 2014
 */

namespace backend\controllers;


use common\models\Municipality;
use common\models\MunicipalityCommon;
use common\models\MunicipalityScope;
use common\models\NormativeLegalSupport;
use common\models\OrgStructure;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class MoformController extends Controller{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Вывод формы пустой сферы деятельности
     * обязательно должны быть параметры: cnt - номер cферы, newName - название сферы из модалки
     * @return string
     */
    public function actionEmptyscope(){
        //todo проверка на обязательность входящих параметров
        $scopeCnt = \Yii::$app->request->post('cnt');
        $newName = \Yii::$app->request->post('newName');
        $scope = new MunicipalityScope();
        $scope->scope_name = $newName;
        return $this->renderPartial('_mscope',['mscopes'=>[$scopeCnt=>$scope]]);
    }

    /**
     * Вывод на редактирование и отправка на сохранение
     * @param $id - id МО
     * @return string
     */
    public function actionEdit($id){
        $post = \Yii::$app->request->post();
        /** @var Municipality $municipality */
        $municipality = Municipality::find()->where(['id'=>$id])->one();
        if(\Yii::$app->user->id != $municipality->user_id){
            $this->redirect('/site/moview/'.$id);
        }
        if(! empty($post)){//если есть post - то пришли данные из формы
            //перед записью пришедших данных очистка предыдущих
            MunicipalityCommon::deleteAll('municipality_id = :id',[':id'=>$id]);
            NormativeLegalSupport::deleteAll('municipality_id = :id',[':id'=>$id]);
            OrgStructure::deleteAll('municipality_id = :id',[':id'=>$id]);
            MunicipalityScope::deleteAll('municipality_id = :id',[':id'=>$id]);
            $this->saveScopeForm($municipality);
            $this->redirect('/');
        }else{
            //если нет post - вывод формы редактирования
            return $this->render('index',[
                'municipality' => $municipality,
                'mscopes' => $municipality->municipalityScopes,
                'municipalityCommon'=>$municipality->municipalityCommons[0],
                'normativeLegal' => $municipality->normativeLegalSupports[0],
                'orgStructure' => $municipality->orgStructures[0]
            ]);
        }
    }

    /**
     * Вывод для создания и отправка на сохранение
     * @return string
     */
    public function actionCreate(){
        $post = \Yii::$app->request->post();
        if(! empty ($post)){

            $this->saveScopeForm();
            $this->redirect('/');
        }else{
            //все параметры для вида новые и пустые
            $municipality = new Municipality();
            $mscopes = [];
            $municipalityCommon = new MunicipalityCommon();
            $normativeLegal = new NormativeLegalSupport();
            $orgStructure = new OrgStructure();

            return $this->render('index',[
                'municipality' => $municipality,
                'mscopes' => $mscopes,
                'municipalityCommon'=>$municipalityCommon,
                'normativeLegal' => $normativeLegal,
                'orgStructure' => $orgStructure
            ]);
        }
    }

    /**
     * Сохранение
     * @param null|Municipality $municipality
     */
    private function saveScopeForm($municipality = null){
        //todo сохранять с линковкой
        $post = \Yii::$app->request->post();

        //сохранение
        if(! $municipality){
            $municipality = new Municipality();
            $municipality->user_id = \Yii::$app->user->id;
        }

        $munEmblem = UploadedFile::getInstance($municipality,'municipality_emblem');
        if($munEmblem){
            $municipality->municipality_emblem = $munEmblem;
            $municipality->municipality_emblem->saveAs('uploads/'.$municipality->municipality_emblem->baseName . '.' . $municipality->municipality_emblem->extension);
        }

        unset($post['Municipality']['municipality_emblem']);

        $municipality->attributes = $post['Municipality'];
        $municipality->save(false);
        //общие данные
        $municipalityCommon = new MunicipalityCommon();
        $municipalityCommon->attributes = $post['MunicipalityCommon'];
        $municipalityCommon->municipality_id = $municipality->id;
        $municipalityCommon->save(false);
        //нормативно правовой
        $normativeLegal = new NormativeLegalSupport();
        $normativeLegal->attributes = $post['NormativeLegalSupport'];
        $normativeLegal->municipality_id = $municipality->id;
        $normativeLegal->save(false);
        //оргструктура
        $orgStructure = new OrgStructure();
        $orgStructure->attributes = $post['OrgStructure'];
        $orgStructure->municipality_id = $municipality->id;
        $orgStructure->save(false);
        //сферы деятельности
        if(isset($post['MunicipalityScope'])){
            foreach($post['MunicipalityScope'] as $scopeArr){
                $scope = new MunicipalityScope();
                //множественные поля, типа основных функций

                if(is_array($scopeArr['org_str'])){
                    foreach($scopeArr['org_str'] as $orgStrArr){
                        $orgStr = new \common\components\sphereparts\OrgStructure();
                        $orgStr->setFields($orgStrArr);
                        $scope->scopeComponents(MunicipalityScope::ORG_STR,$orgStr);
                    }
                }

                if (is_array($scopeArr['bas_func'])) {
                    foreach ($scopeArr['bas_func'] as $basFuncArr) {
                        $basFunc = new \common\components\sphereparts\BasicFunction();
                        $basFunc->setFields($basFuncArr);
                        $scope->scopeComponents(MunicipalityScope::BAS_FUNC, $basFunc);
                    }
                }

                if (is_array($scopeArr['scope_serv'])) {
                    foreach ($scopeArr['scope_serv'] as $scopeServArr) {
                        $scopeServ = new \common\components\sphereparts\ScopeService();
                        $scopeServ->setFields($scopeServArr);
                        $scope->scopeComponents(MunicipalityScope::SCOPE_SERV, $scopeServ);
                    }
                }

                if (is_array($scopeArr['scope_info'])) {
                    foreach ($scopeArr['scope_info'] as $scopeInfoArr) {
                        $scopeInfo = new \common\components\sphereparts\InfoObject();
                        $scopeInfo->setFields($scopeInfoArr);
                        $scope->scopeComponents(MunicipalityScope::INFO_OBJ, $scopeInfo);
                    }
                }

                if (is_array($scopeArr['sc_reports'])) {
                    foreach ($scopeArr['sc_reports'] as $scopeRepArr) {
                        $scopeRep = new \common\components\sphereparts\ScopeReport();
                        $scopeRep->setFields($scopeRepArr);
                        $scope->scopeComponents(MunicipalityScope::SCOPE_REP, $scopeRep);
                    }
                }
                //убираем заполненные множественные поля для назначения оставшихся простых полей
                unset($scopeArr['org_str']);
                unset($scopeArr['bas_func']);
                unset($scopeArr['scope_serv']);
                unset($scopeArr['scope_info']);
                unset($scopeArr['sc_reports']);

                $scope->attributes = $scopeArr;
                $scope->scope_recomendations = $scopeArr['scope_recomendations'];

                $scope->municipality_id = $municipality->id;
                $scope->save(false);
            }
        }
    }

    public function actionIndex(){
        return $this->render('index');
    }

}