<?php
/**
 * Тайл МО
 * (для вывода в лобби - название, изображение, дата и т.п.)
 * MOTile.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 1/10/15
 * Time: 3:28 AM
 * Copyright 2015
 */

namespace common\components\front;


class MOTile {

    private $linkBase = '/site/moview/';
    private $imgFolder;
    private $img;
    private $id;
    private $name;
    private $date;

    /**
     * @return string
     */
    public function getImg() {
        return '/uploads/'.$this->img;
    }

    /**
     * @param string $img
     */
    public function setImg($img) {
        $this->img = $img;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date) {
        $this->date = $date;
    }

    public function getId(){
        return $this->id;
    }


    function __construct($id,$linkBase = null) {
        $this->id = $id;
        if($linkBase){
            $this->linkBase = $linkBase;
        }
    }

    /**
     * @return string
     */
    public function getLink() {
        return $this->linkBase.$this->id;
    }



}