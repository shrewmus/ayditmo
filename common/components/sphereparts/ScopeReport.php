<?php
/**
 * Предоставляемый отчетный документ
 * ScopeReport.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 6:34 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


class ScopeReport extends SphereSubrow{

    /** @var string Название отчета */
    protected $reportName;
    /** @var string Краткие сведения о составе данных предоставляемых в отчете */
    protected $reportInfo;
    /** @var  string Кому предоставляется, название РОИВ/ФОИВ */
    protected $reportConsumer;
    /** @var  string Форма предоставления, ИС, по форме, в бумажном виде */
    protected $reportFormat;
    /** @var  string Периодичность предоставления отчета, месяц, квартал, год, по запросу */
    protected $reportPeriod;
    /** @var  string Основание для предоставления отчета, № ФЗ, постановление/распоряжение, письмо и т.д. */
    protected $reportBasis;

    /**
     * Массив полей в виде имяполя=>значение поля
     *
     * @return array
     */
    function asArray() {
        return [
            'reportName' => $this->reportName,
            'reportInfo' => $this->reportInfo,
            'reportConsumer' => $this->reportConsumer,
            'reportFormat' => $this->reportFormat,
            'reportPeriod' => $this->reportPeriod,
            'reportBasis' => $this->reportBasis
        ];
    }

    /**
     * Массив меток для полей в виде имяполя=>метка
     *
     * @return array
     */
    function getLabels() {
        return [
            'reportName' => 'Название отчета',
            'reportInfo' => 'Краткие сведения о составе данных предоставляемых в отчете',
            'reportConsumer' => 'Кому предоставляется, название РОИВ/ФОИВ',
            'reportFormat' => 'Форма предоставления, ИС, по форме, в бумажном виде',
            'reportPeriod' => 'Периодичность предоставления отчета, месяц, квартал, год, по запросу',
            'reportBasis' => 'Основание для предоставления отчета, № ФЗ, постановление/распоряжение, письмо и т.д.'
        ];
    }}