<?php
/**
 * SphereSubrow.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 4:57 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


abstract class SphereSubrow {

    protected $possibleValues = [];

    /**
     * Установка значения поля
     * @param string $fieldName имя поля
     * @param string $fieldValue значение поля
     */
    public function setField($fieldName,$fieldValue){
        $this->$fieldName = $fieldValue;
    }

    /**
     * Установка значений нескольких полей из массива, где ключи - имена полей
     * @param array $fields
     */
    public function setFields(array $fields){
        foreach($fields as $fieldName=>$fieldValue){
            $this->$fieldName = $fieldValue;
        }
    }

    public function getField($fieldName){
        $arr = $this->asArray();
        if(isset ($arr[$fieldName])){
            return $arr[$fieldName];
        }
    }

    /**
     * Возвращается массив вариантов значений для поля(если поле - дропдаун или т.п.)
     * или ложь если вариантов значений нет
     * @param string $fieldName имя поля
     * @return boolean|array
     */
    public function getFieldPossibleValue($fieldName){
        if(isset($this->possibleValues[$fieldName])){
            return $this->possibleValues[$fieldName];
        }else{
            return false;
        }
    }

    /**
     * Массив полей в виде имяполя=>значение поля
     * @return array
     */
    abstract function asArray();

    /**
     * Массив меток для полей в виде имяполя=>метка
     * @return array
     */
    abstract function getLabels();
}