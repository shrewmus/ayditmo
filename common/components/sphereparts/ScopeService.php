<?php
/**
 * Предоставляемая муниципальная услуга сферы деятельности
 * MunicipalServices.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 5:53 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


class ScopeService extends SphereSubrow {

    /** @var string  Наименование муниципальной услуги */
    protected $serviceName;
    /** @var  string Реквизиты административного регламента предоставления МУ (дата принятия) */
    protected $regulationDetail;
    /** @var string Фактические адреса мест предоставления для каждой услуги, город, улица, дом, кабинет, кол-во раб.мест (за исключением МФЦ) */
    protected $serviceAddress;
    /** @var int Предоставляется в электронном виде на РПГУ, этап по 1993-р */
    protected $servNum1993;
    /** @var  boolean Предоставляются в МФЦ */
    protected $isMFCService;

    protected $possibleValues = [
        'servNum1993'=>[
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '0'=>'не предоставл.'
        ],
        'isMFCService'=>[
            'false'=>'нет',
            'true'=>'да'
        ]
    ];

    /**
     * Массив полей в виде имяполя=>значение поля
     *
     * @return array
     */
    public function asArray() {
        return [
            'serviceName'      => $this->serviceName,
            'regulationDetail' => $this->regulationDetail,
            'serviceAddress' => $this->serviceAddress,
            'servNum1993' => $this->servNum1993,
            'isMFCService' => $this->isMFCService
        ];
    }

    /**
     * Массив меток для полей в виде имяполя=>метка
     *
     * @return array
     */
    function getLabels() {
        return [
            'serviceName'      => 'Наименование муниципальной услуги',
            'regulationDetail' => 'Реквизиты административного регламента предоставления МУ (дата принятия)',
            'serviceAddress' => 'Фактические адреса мест предоставления для каждой услуги, город, улица, дом, кабинет, кол-во раб.мест (за исключением МФЦ)',
            'servNum1993' => 'Предоставляется в электронном виде на РПГУ, этап по 1993-р',
            'isMFCService' => 'Предоставляются в МФЦ'
        ];
    }}