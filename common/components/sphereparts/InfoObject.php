<?php
/**
 * Объект учета, данные и информация, с которой работает подразделение
 * InfoObject.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 6:29 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


class InfoObject extends SphereSubrow{

    /** @var  string Название объекта учета/данные */
    protected $objectName;
    /** @var  string В какой системе ведется реестр/СУБД */
    protected $sysName;
    /** @var string Подразделение ответственное за актуальность данных (владелец данных, в том числе РОИВ, ФОИВ и т.д.) */
    protected $responseDepartment;
    /** @var  string Примечание */
    protected $note;

    /**
     * Массив полей в виде имяполя=>значение поля
     *
     * @return array
     */
    function asArray() {
        return [
            'objectName' => $this->objectName,
            'sysName' => $this->sysName,
            'responseDepartment' => $this->responseDepartment,
            'note' => $this->note
        ];
    }

    /**
     * Массив меток для полей в виде имяполя=>метка
     *
     * @return array
     */
    function getLabels() {
        return [
            'objectName' => 'Название объекта учета/данные',
            'sysName' => 'В какой системе ведется реестр/СУБД',
            'responseDepartment' => 'Подразделение ответственное за актуальность данных (владелец данных, в том числе РОИВ, ФОИВ и т.д.)',
            'note' => 'Примечание'
        ];
    }
}