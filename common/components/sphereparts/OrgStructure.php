<?php
/**
 * Организационная структура
 * OrgStructure.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 5:55 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


class OrgStructure extends SphereSubrow{

    /** @var string Отдел */
    protected $department;
    /** @var string ФИО */
    protected $fio;
    /** @var string Количество */
    protected $count;


    /**
     * Массив полей в виде имяполя=>значение поля
     *
     * @return array
     */
    function asArray() {
        return [
            'department'=>$this->department,
            'fio'=>$this->fio,
            'count'=>$this->count
        ];
    }

    /**
     * Массив меток для полей в виде имяполя=>метка
     *
     * @return array
     */
    function getLabels() {
        return [
            'department'=>'Отдел',
            'fio'=>'ФИО',
            'count'=>'Количество'
        ];
    }}