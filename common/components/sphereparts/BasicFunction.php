<?php
/**
 * Основная функция сферы деятельности
 * BasicFunction.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 4:57 PM
 * Copyright 2014
 */

namespace common\components\sphereparts;


class BasicFunction extends  SphereSubrow{

    /** @var string Наименование полномочий/функций (задачи) */
    protected $taskName;
    /** @var string Название ИС/ПО используемых для автоматизации полномочий/функций */
    protected $isysName;
    /** @var string Код ИС по классификатору (приложение 0) */
    protected $isysCode;

    /**
     * Массив меток для полей в виде имяполя=>метка
     *
     * @return array
     */
    function getLabels() {
        return [
            'taskName'=>'Наименование полномочий/функций (задачи)',
            'isysName'=>'Название ИС/ПО используемых для автоматизации полномочий/функций',
            'isysCode'=>'Код ИС по классификатору (приложение 0)'
        ];
    }

    /**
     * Массив полей в виде имяполя=>значение поля
     *
     * @return array
     */
    function asArray() {
        return [
            'taskName'=>$this->taskName,
            'isysName'=>$this->isysName,
            'isysCode'=>$this->isysCode
        ];
    }}