<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "org_structure".
 *
 * @property integer $id
 * @property string $approved_by
 * @property integer $committee_count
 * @property integer $committee_regular
 * @property integer $teradmin_count
 * @property integer $teradmin_regular
 * @property integer $admin_count
 * @property integer $admin_regular
 * @property integer $department_count
 * @property integer $department_regular
 * @property integer $all_regular_count
 * @property string $informations_authority
 * @property string $recommendations
 * @property integer $municipality_id
 *
 * @property Municipality $municipality
 */
class OrgStructure extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_structure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approved_by', 'informations_authority', 'orgstruct_recommendations'], 'string'],
            [['committee_count', 'committee_regular', 'teradmin_count', 'teradmin_regular', 'admin_count', 'admin_regular', 'department_count', 'department_regular', 'all_regular_count', 'municipality_id'], 'integer'],
            [['municipality_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orgstruct_id' => 'ID',
            'approved_by' => 'Кем принята',
            'committee_count' => 'количество',
            'committee_regular' => 'штатная численность',
            'teradmin_count' => 'количество',
            'teradmin_regular' => 'штатная численность',
            'admin_count' => 'количество',
            'admin_regular' => 'штатная численность',
            'department_count' => 'количество',
            'department_regular' => 'штатная численность',
            'all_regular_count' => 'Общее штатное количество муниципальных служащих',
            'informations_authority' => 'Полномочия и курирование ИКТ',
            'orgstruct_recommendations' => 'Рекомендации',
            'municipality_id' => 'Municipality ID',
        ];
    }

    /**
     * Дополнительное разделение подписей к полям
     * для случая разбиения составных полей типа departament_count
     * @return array
     */
    public function rowAttributeLabels(){
        return[
            'pairs'=>['count'=>'кол-во','regular'=>'штатная числ-ть'],
            'rows' => [
                'committee' =>'Комитеты',
                'teradmin' => 'Терр..е управления',
                'admin' => 'Управления',
                'department' => 'Отделы'
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'municipality_id']);
    }
}
