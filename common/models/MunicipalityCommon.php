<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "municipality_common".
 *
 * @property integer $municipality_id
 * @property string $customer
 * @property string $executor
 * @property string $work_description
 * @property string $goals
 * @property string $tasks
 * @property string $work_list
 * @property string $survey_methods
 * @property string $survey_borders
 * @property string $abbreviations
 * @property integer $id
 *
 * @property Municipality $municipality
 */
class MunicipalityCommon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipality_common';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['municipality_id'], 'required'],
            [['municipality_id'], 'integer'],
            [['customer', 'executor', 'work_description', 'goals', 'tasks', 'work_list', 'survey_methods', 'survey_borders', 'abbreviations'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'municipality_id' => 'Municipality ID',
            'customer' => 'Заказчик',
            'executor' => 'Исполнитель',
            'work_description' => 'Полное наименование работ',
            'goals' => 'Цели',
            'tasks' => 'Задачи',
            'work_list' => 'Краткий состав работ',
            'survey_methods' => 'Методика обследования',
            'survey_borders' => 'Границы обследования',
            'abbreviations' => 'Сокращения',
            'mcommon_id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'municipality_id']);
    }


}
