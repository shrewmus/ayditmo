<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "municipality_scope".
 *
 * @property integer $scope_id
 * @property integer $municipality_id
 * @property string $scope_name
 * @property string $curator
 * @property string $development_authority
 * @property integer $regular_count
 * @property string $org_structure
 * @property string $interview_info
 * @property string $basic_functions
 * @property string $scope_services
 * @property string $info_objects
 * @property string $scope_reports
 * @property string $conclusion
 * @property string $scope_recomendations
 *
 * @property Municipality $municipality
 */
class MunicipalityScope extends ActiveRecord
{

    const ORG_STR = 'orgStructures';
    const BAS_FUNC = 'basicFunctions';
    const SCOPE_SERV = 'scopeServices';
    const INFO_OBJ = 'infoObjects';
    const SCOPE_REP = 'scopeReports';

    private $orgStructures = [];
    private $basicFunctions = [];
    private $scopeServices = [];
    private $infoObjects = [];
    private $scopeReports = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipality_scope';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['municipality_id', 'scope_name'], 'required'],
            [['municipality_id', 'regular_count'], 'integer'],
            [['development_authority', 'org_structure', 'interview_info', 'basic_functions','scope_services', 'info_objects', 'scope_reports', 'conclusion', 'recomendations'], 'string'],
            [['scope_name', 'curator'], 'string', 'max' => 255]
        ];
    }

    public function scopeComponents($componentName,$componentToAdd){
        $arr = $this->$componentName;
        array_push($arr,$componentToAdd);
        $this->$componentName = $arr;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scope_id' => 'ID',
            'municipality_id' => 'Municipality ID',
            'scope_name' => 'Название сферы деятельности МО',
            'curator' => 'Куратор:',
            'development_authority' => 'Полномочия по развитию:',
            'regular_count' => 'Штатная численность:',
            'org_structure' => 'Организационная структура',
            'interview_info' => 'Информация, полученная в результате интерьвю:',
            'basic_functions' => 'Основные функции:',
            'scope_services' => 'Предоставляемы муниципальные услуги',
            'info_objects' => 'Объекты учета, данные и информация, с которой работает подразделение',
            'scope_reports' => 'Предоставляемые отчетные документы',
            'conclusion' => 'Заключение',
            'scope_recomendations' => 'Рекомендации',
        ];
    }

    /**
     * Перед сохранением массивы с объектами сериализовать в строковые поля
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert) {
            $fields = ['org_structure'=>'orgStructures','basic_functions'=>'basicFunctions','scope_services'=>'scopeServices','info_objects'=>'infoObjects','scope_reports'=>'scopeReports'];
            foreach ($fields as $key => $value) {
                if(!empty($this->$value)){
                    $this->$key = serialize($this->$value);
                }
            }
            return parent::beforeSave($insert);

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'municipality_id']);
    }

    // -------- востановленные массивы объектов ------------

    /**
     * @return array of \common\components\sphereparts\OrgStructure
     */
    public function getOrgStructures(){
        return $this->getUnserialized('org_structure','orgStructures');
    }

    /**
     * @return array of \common\components\sphereparts\BasicFunctions
     */
    public function getBasicFunctions(){
        return $this->getUnserialized('basic_functions','basicFunctions');
    }

    /**
     * @return array of \common\components\sphereparts\ScopeService
     */
    public function getScopeServices(){
        return $this->getUnserialized('scope_services','scopeServices');
    }

    public function getInfoObjects(){
        return $this->getUnserialized('info_objects','infoObjects');
    }

    public function getScopeReports(){
        return $this->getUnserialized('scope_reports','scopeReports');
    }

    /**
     * Восстановление сериализованных данных
     * @param $strField string имя строковго поля из бд с сериализованным массивом объектов
     * @param $arrayField string имя поля-массива объектов
     * @return array|mixed
     */
    private function getUnserialized($strField,$arrayField){
        if(trim($this->$strField) !== ''){
            if(empty($this->$arrayField)){
                $this->$arrayField = unserialize($this->$strField);
            }
            return $this->$arrayField;
        }else{
            return [];
        }
    }
}
