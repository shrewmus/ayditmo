<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "municipality".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $municipality_name
 * @property string $municipality_emblem
 * @property string $percent
 * @property string $last_edit_at
 * @property string $scope_prepdoc
 *
 * @property MunicipalityCommon[] $municipalityCommons
 * @property MunicipalityScope[] $municipalityScopes
 * @property NormativeLegalSupport[] $normativeLegalSupports
 * @property OrgStructure[] $orgStructures
 */
class Municipality extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['municipality_name'], 'required'],
            [['last_edit_at'], 'safe'],
            [['scope_prepdoc'], 'string'],
            [['municipality_emblem'],'file'],
            [['municipality_name', 'percent'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Author ID',
            'municipality_name' => 'Название Муниципалитета',
            'municipality_emblem' => 'Municipality Emblem',
            'percent' => '%',
            'last_edit_at' => 'Last Edit At',
            'scope_prepdoc' => 'Подготовка документов к обследованию:',
        ];
    }

    public function getNames(){
        $classes= ['common\models\MunicipalityCommon','common\models\NormativeLegalSupport','common\models\OrgStructure','common\models\MunicipalityScope'];
        $toUnset = ['id','user_id','last_edit_at','percent','municipality_id','mcommon_id','legal_id','orgstruct_id','scope_id'];
        $names[] = $this->attributeLabels();
        $names[0]['municipality_emblem'] = '';
        $names[0] = array_merge(array_slice($names[0], 0, 3), array_slice($names[0], 3));
        foreach($classes as $class){
            /** @var ActiveRecord $cl */
            $cl = new $class();
            if($class != 'common\models\MunicipalityScope' ){
                $partNames = $cl->attributeLabels();
            }else{
                $partNames  = $cl->attributeLabels();
            }
            $names[] = $partNames;
        }
        foreach ($toUnset as $unsetKey) {
            foreach($names as $nk=>$partNames){
                if(isset($partNames[$unsetKey])){
                    unset($names[$nk][$unsetKey]);
                }
            }
        }
        $names[4]['scope_prepdoc'] = $names[0]['scope_prepdoc'];
        unset($names[0]['scope_prepdoc']);
        return $names;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipalityCommons()
    {
        return $this->hasMany(MunicipalityCommon::className(), ['municipality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipalityScopes()
    {
        return $this->hasMany(MunicipalityScope::className(), ['municipality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNormativeLegalSupports()
    {
        return $this->hasMany(NormativeLegalSupport::className(), ['municipality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgStructures()
    {
        return $this->hasMany(OrgStructure::className(), ['municipality_id' => 'id']);
    }

    /**
     * Получение массива полей разделов
     * вид массива для поля [key=> name=> value=> (value|same array)]
     * @return array
     */
    public function getParts(){
        $parts = [
            [
                'key'=>'common',
                'partName' => '1. ОБЩИЕ СВЕДЕНИЯ',
                'partRows' => $this->prepSimpleFields($this->municipalityCommons[0], ['municipality_id', 'mcommon_id']),
            ],
            [
                'key' => 'legal',
                'partName' => '2. НОРМАТИВНО-ПРАВОВОЕ ОБЕСПЕЧЕНИЕ',
                'partRows' => $this->prepSimpleFields($this->normativeLegalSupports[0],['municipality_id','legal_id']),
            ],
            [
                'key'=>'orgstr',
                'partName' => '3. ОРГАНИЗАЦИОННО-ШТАТНАЯ СТРУКТУРА',
                'partRows' => $this->getOrgStrFields()
            ],
            [
                'key'=>'scopes',
                'partName' => '4. ИНФОРМАТИЗАЦИЯ СФЕР ДЕЯТЕЛЬНОСТИ',
                'partRows' => $this->getScopes()
            ]
        ];

        return $parts;
    }

    /**
     * Получение массива для поля с простым значением
     * @param ActiveRecord  $model
     * @param array $keysToUnset
     *
     * @return array
     */
    private function prepSimpleFields($model,$keysToUnset = []){
        $retArr = [];
        $attributes =$model->attributes;
        $labels = $model->attributeLabels();
        foreach($keysToUnset as $key){
            unset($attributes[$key]);
        }
        foreach($attributes as $key=>$value){
            $retArr[$key]=[
                'key' => $key,
                'name' => $labels[$key],
                'value'=> $value
            ];
        }
        return $retArr;
    }

    /**
     * Получение массива для составных полей для оргструктуры (3 разел)
     * @return array
     */
    private function getOrgStrFields(){
        $orgLabeled = $this->prepSimpleFields($this->orgStructures[0],['municipality_id','orgstruct_id']);
        $additionalLabels = $this->orgStructures[0]->rowAttributeLabels();
        $newKeyMapping = [];
        foreach($orgLabeled as $key=>$orgField){
            $partKey = explode("_",$orgField['key']);
            if(array_key_exists($partKey[0],$additionalLabels['rows'])){
                if(array_key_exists($partKey[0],$newKeyMapping)){
                    $orgLabeled[$newKeyMapping[$partKey[0]]]['value'][]=[
                        'key'=>$partKey[1],
                        'value'=> $orgField['value'],
                        'name' => $orgField['name']
                    ];
                    unset($orgLabeled[$key]);
                }else{
                    $newKeyMapping[$partKey[0]] = $key;
                    $orgLabeled[$key]=[
                        'key'=>$partKey[0],
                        'name'=>$additionalLabels['rows'][$partKey[0]],
                        'value' => [
                            [
                                'key'   => $partKey[1],
                                'value' => $orgField['value'],
                                'name'  => $orgField['name']
                            ]
                        ]
                    ];
                }
            }
        }
        return $orgLabeled;
    }

    /**
     * Получение массива значений для сферы
     * (с учетом множесвтенных полей)
     * @return array
     */
    private function getScopes(){
        $scopes = $this->municipalityScopes;
        $labeledScopes = [];
        $filterKeys = ['org_structure','basic_functions','scope_services','info_objects','scope_reports'];
        /** @var MunicipalityScope $scope */
        foreach($scopes as $scope){
            $labeled = $this->prepSimpleFields($scope,['scope_id','municipality_id']);
            // todo переделать чтобы несколько элементов внутри одного поля, а не несколько повторяющихся полей как сейчас
            foreach($labeled as $idx => $field){
                if(in_array($field['key'],$filterKeys)){
                    $labeled[$idx]['value'] = unserialize($field['value']);
                }
            }
            $labeledScopes[] = $labeled;
        }
        return $labeledScopes;
    }

}
