<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "normative_legal_support".
 *
 * @property integer $id
 * @property string $survey_result
 * @property string $recommendations
 * @property integer $municipality_id
 *
 * @property Municipality $municipality
 */
class NormativeLegalSupport extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'normative_legal_support';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['survey_result', 'legal_recommendations'], 'string'],
            [['municipality_id'], 'required'],
            [['municipality_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'legal_id' => 'ID',
            'survey_result' => 'Выявлено в ходе обследования',
            'legal_recommendations' => 'Рекомендации',
            'municipality_id' => 'Municipality ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipality()
    {
        return $this->hasOne(Municipality::className(), ['id' => 'municipality_id']);
    }
}
