<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'layout' => '@common/views/layouts/formLayout',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
