<?php
/**
 * formLayout.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12/28/14
 * Time: 7:15 PM
 * Copyright 2014
 */
use backend\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

/** @var $this \yii\web\View */

AppAsset::register($this);

$this->beginPage(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        $menuItems = [];
        if (Yii::$app->user->isGuest) {
            $menuItems = [
							['label' => 'Главная', 'url' => ['/'], 'linkOptions' => ['style' => 'float: left;']],
                ['label' => 'Вход', 'url' => ['/site/login'], 'options' => ['style' => 'float: right;']],
                /*['label' => 'Регистрация','url' => ['/site/signup']]*/
            ];
        } else {
            $menuItems[] = ['label'       => 'Выйти',
                            'url'         => ['/site/logout'],
														'options' => ['style' => 'float: right;'],
                            'linkOptions' => ['data-method' => 'post']];
        }
        NavBar::begin();
        echo Nav::widget(
            [
                'options' => ['class' => 'navbar-nav navbar-left', 'style' => 'width: 100%'],
                'items'   => $menuItems
            ]
        );
        NavBar::end();
        ?>
        <div class="container">
            <?= $content ?>
        </div>
    </div>
		<div class="footer-container container">
			<div style="float: right; padding: 20px; 0; margin-right: -51px;">
				<img src="/img/u0_normal.png" style="float: left; width: 32px; vertical-align: middle; margin-right: 5px; margin-top: -1px;" />
				<span style="font-size: 12px;">
					ГАУ "РИАЦ"<br />
					&copy; <?=date('Y');?>
				</span>
			</div>
		</div>
    <?php $this->endBody();
    $this->endPage(); ?>
</body>
</html>